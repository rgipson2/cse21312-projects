/*************************************
 * File name: Section01_LL_NC.cc 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * This file is a test of the LinkedList template 
 * class, with two functions for removing removing 
 * duplicates, where the node is a class 
 * ***********************************/

#include <iostream>
#include <unordered_map>
#include "Section01_LinkedList_NodeClass.h"

/****************************
* Function Name: removeDuplicates_1
* Preconditions: LinkedList<T>* theList
* Postconditions: none 
* This function takes a pointer to a linked list 
* and removes all duplicates in the list.
* First implementation uses undordered_map run time is O(n) with O(n) space
*****************************/
template<class T>
void removeDuplicates_1(LinkedList<T>* theList)
{
    if (theList->getHeadNode() == NULL)
    {
        return;
    }

    std::unordered_map<T,bool> map;
    Node<T>* current = theList->getHeadNode();
    map[current->getNodeData()] = 1;

    while (current->getNodeLink())
    {
        if (map[current->getNodeLink()->getNodeData()] == 0)
        {
            map[current->getNodeLink()->getNodeData()] = 1;
            current = current->getNodeLink();
        }
        else
        {
            //current->removeElementO_n_Space(current);
            theList->removeNode(current);
        }
    }
}

/****************************
* Function Name: removeDuplicates_1
* Preconditions: LinkedList<T>* theList
* Postconditions: none 
* This function takes a pointer to a linked list 
* and removes all duplicates in the list.
* This implementation uses no additional data structure run time is O(n^2) with O(1) space
*****************************/
template<class T>
void removeDuplicates_2(LinkedList<T>* theList)
{
    Node<T>* head = theList->getHeadNode();
    
    if (theList->getHeadNode() == NULL)
    {
        return;
    }

    Node<T>* current = head;
    Node<T>* runner;

    while (current)
    {
        runner = current;
        
        while (runner->getNodeLink())
        {
            if (current->getNodeData() == runner->getNodeLink()->getNodeData())
            {
                theList->removeNode(runner);
            }
            else
            {
                runner = runner->getNodeLink();
            }
        }

        current = current->getNodeLink();
    }
}

/****************************
* Function Name: main 
* Preconditions: int, char** 
* Postconditions: int 
* This is the main driver function.
*****************************/
int main(int argc, char**argv)
{
    LinkedList<int>* myList = new LinkedList<int>();
    myList->insert(5);
    myList->insert(7);
    myList->insert(12);
    myList->insert(7);
    myList->insert(16);
    myList->insert(16);
    myList->insert(25);
    myList->insert(16);
    myList->insert(25);
    myList->insert(11);
    myList->insert(19);

    std::cout << "The original list is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;

    // Runs removeDuplicates_1 to run that implementation
    removeDuplicates_1(myList);
    std::cout << "The list with removeDuplicates_1 duplicated removed is: ";
    myList->display();   
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    myList->insert(19);
    myList->insert(25);
    myList->insert(11);
    myList->insert(5);    
    myList->insert(7);  
    
    std::cout << "The new original list is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    // Runs removeDuplicates_2 to run that implementation
    removeDuplicates_2(myList);

    std::cout << "The new list with removeDuplicates_2 duplicated removed is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    delete myList;
    
    return 0;
}